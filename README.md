NLP Assignment 3 – Neural Machine Translation   
Description   
For this assignment, you are offered an English-Chinese parallel training data and testing data.    
The task is supervised English-to-Chinese translation.  
You have to use the well-trained model to generate the translated Chinese sentences from English sentences of the testing data (test.txt).  
The format of your output file should be the same as the format of test-sample.tsv. We will evaluate your submissions with ground truth.   
Requirements    
● Python programming language only    
● You can use any machine learning library TensorFlow, Keras, Pytorch, etc.   
● You can use snippets of code from open-source projects, tutorials, blogs, etc. Do not clone the entire projects directly. Try to implement a model by yourself.    
Grading  
Models are evaluated based on BLEU score of the testing data.   
● Completed Source Code (50%)   
● BLEU-Score (40%)   
● Report (10%)    
Submission Rule    
Please pack up the following files into a .zip file named as studentid_hw3.zip and upload it to LMS:   
● Python source code .py (one or more).    
● test.tsv: test.tsv with translated Chinese sentences    
● requirements.txt: installed libraries in your Python environment. To create a requirements file, use:    
pip freeze > requirements.txt   
● report.pdf: assignment report. You can write anything you want.   
 
Deadline: 2019/12/14   