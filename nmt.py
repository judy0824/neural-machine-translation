# -*- coding: utf-8 -*-
"""NMT.ipynb

Automatically generated by Colaboratory.

Original file is located at
    https://colab.research.google.com/drive/14ExFbQEAU1l2_U45Z6FvCV4f_R1nxder
"""

from google.colab import drive
drive.mount('/content/drive')

train_df = open('/content/drive/My Drive/IISR/Neural Machine Translation/train.tsv', encoding='utf-8').readlines()
test_df = open('/content/drive/My Drive/IISR/Neural Machine Translation/test.txt', encoding='utf-8').readlines()

English_sents = []
Chinese_sents = []
test_sents = []
for sent in train_df:
  s = sent.split('\t')
  English_sents.append(s[0].lower())
  Chinese_sents.append(s[1][0:-1])
for sent in test_df:
  test_sents.append(sent.lower())

import nltk
from nltk import word_tokenize
from nltk.tokenize import RegexpTokenizer

PAD_TOKEN = 0
UNK_TOKEN = 1
tokenizer = RegexpTokenizer(r'[’]?\w+')
english_word_dict = {'<PAD>': PAD_TOKEN, '<UNK>': UNK_TOKEN}
def get_word_dict(word_dict, word):
  if not (word in word_dict):
    word_dict[word] = len(word_dict)
  return word_dict
for i, e in enumerate(English_sents):
  words = tokenizer.tokenize(e)
  for w in words:
    english_word_dict = get_word_dict(english_word_dict, w)

def word2idx(word_dict, word):
  if not (word in word_dict):
    return(word_dict['<UNK>'])
  else:
    return(word_dict[word])
e_idx2word = {i:w for w, i in english_word_dict.items()}
def index2word(idx2word, index):
  return(idx2word[index])

sent_in_idx = []
all_english_idx_sents = []
test_idx_sents = []
for e in English_sents:
  words = tokenizer.tokenize(e)
  for w in words:
    sent_in_idx.append(word2idx(english_word_dict, w))
  all_english_idx_sents.append(sent_in_idx)
  sent_in_idx = []
sent_in_idx = []
for e in test_sents:
  words = tokenizer.tokenize(e)
  for w in words:
    sent_in_idx.append(word2idx(english_word_dict, w))
  test_idx_sents.append(sent_in_idx)
  sent_in_idx = []

import jieba
import re

chinese_word_dict = {'<PAD>': PAD_TOKEN, '<UNK>': UNK_TOKEN}
for c in Chinese_sents:
  c = re.sub(r'[^\u4e00-\u9fa5]', '', c) 
  words = jieba.cut(c, cut_all=False)
  for w in words:
    chinese_word_dict = get_word_dict(chinese_word_dict, w)

all_chinese_idx_sents = []
c_idx2word = {i:w for w, i in chinese_word_dict.items()}
for c in Chinese_sents:
  c = re.sub(r'[^\u4e00-\u9fa5]', '', c) 
  words = jieba.cut(c, cut_all=False)
  for w in words:
    sent_in_idx.append(word2idx(chinese_word_dict, w))
  all_chinese_idx_sents.append(sent_in_idx)
  sent_in_idx = []

english_max = 0
for s in all_english_idx_sents:
  if(len(s) > english_max):
    english_max = len(s)
print(english_max)
chinese_max = 0
for s in all_chinese_idx_sents:
  if(len(s) > chinese_max):
    chinese_max = len(s)
print(chinese_max)

import keras
import numpy as np
from numpy import newaxis
from keras.preprocessing.sequence import pad_sequences
from keras.models import Sequential
from keras.layers import GRU, Dense, TimeDistributed, RepeatVector, Embedding, Bidirectional, LSTM

def pad_sent(sents, max_len):
  return pad_sequences(sents, maxlen = max_len, padding = 'post')

english_trainData = pad_sent(all_english_idx_sents, english_max)
chinese_trainData = pad_sent(all_chinese_idx_sents, chinese_max)
testData = pad_sent(test_idx_sents, english_max)
print(english_trainData.shape)

batch_size = 256
epochs = 10

def nmt_model(input_shape, output_sequence_length, english_size, chinese_size):
  model=Sequential()
  model.add(Embedding(input_dim=english_size, output_dim=128, input_length=input_shape[1]))
  model.add(Bidirectional(GRU(256, return_sequences=False)))
  model.add(RepeatVector(output_sequence_length))
  model.add(Bidirectional(GRU(256, return_sequences=True)))
  model.add(TimeDistributed(Dense(chinese_size, activation='softmax')))

  model.compile(loss='sparse_categorical_crossentropy', optimizer="rmsprop", metrics=['accuracy'])
  return model

def train(englishData, chineseData):
  chineseData = chineseData.reshape(*chineseData.shape, 1)
  model = nmt_model(
      englishData.shape,
      chinese_trainData.shape[1],
      len(english_word_dict)+1,
      len(chinese_word_dict)+1)
  model.fit(
      englishData,
      chineseData,
      batch_size=batch_size,
      epochs=epochs,
      validation_split=0.3)
  return model
model = train(english_trainData, chinese_trainData)

testData = pad_sent(test_idx_sents, english_max)
all_predictions = []
for i in range(int(len(testData)/100)):
  prediction = model.predict(np.array(testData[int(len(testData)/100)*i:int(len(testData)/100)*(i+1)]))
  prediction = np.argmax(prediction, axis=-1)
  prediction = [[c_idx2word[i] for i in row] for row in prediction]
  for p in prediction:
    all_predictions.append(p)

for a in all_predictions[:100]:
  print(a)
sentence = ""
sentences = []
for sent in all_predictions:
  for word in sent:
    if(word=='<PAD>'):
      continue
    else:
      sentence = sentence + word
  sentences.append(sentence)
  sentence = ""
print(len(sentences))
output = open('/content/drive/My Drive/IISR/Neural Machine Translation/output.tsv', 'w')
for en_sent, ch_sent in zip(test_df, sentences):
  output.write(en_sent[:-2] + '\t' + ch_sent + '\n')